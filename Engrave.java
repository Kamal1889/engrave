import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/*
 * This class engraves the measures on a measuring cup. 
 * @author Kamaljit Grewal
 *
 */

public class Engrave {
	public ArrayList<String> drawnObject;
	int charPrintCount;

	
	public Engrave() {
		/**
		 * This constructor defines an Engrave object.
		 * @instanceVariable drawnObject. Whenever the code prints a line, the number of printed dashes [and label if applicable] are stored in drawnObject
	   		If the line is labeled with a number, the # of dashes will be followed with a comma, a space and the number that is printed.
		 * @instanceVariable charPrintCount  holds  the  number  of characters that is printed every time that drawLine is called.
		 */
		drawnObject = new ArrayList<String>(); 
		charPrintCount = 0;
	}
	
	
	public void engrave(int tickLength, int magnitude) {
		/**
		 * This method is the recursive method that is called when the button is pressed. Calls methods drawIntervals() and drawLine().
		 * @param tickLength is  the  number  of  dashes  that  is printed next to the numbers. This is previously referred to as the length of the major lines.
		 * @param magnitude points  to  the  capacity  of  the  cup.
		 * @precondition The magnitude of a cup cannot be a negative number.
		 */		
		if (tickLength >= 0) { //engrave Test 5 failed, so I added this if. So we don't get negative tickLength
			if (magnitude == 0) {  //base case to eventually stop recursion
				drawLine(tickLength, (char) magnitude);   //draw the final major line of dashes and print label next to it
				drawnObject.add("[" + String.valueOf(tickLength) + ", " + magnitude + "]"); 
				//comment for above line: add # of dashes and number printed next to it, to drawnObject as [#, #]
			}
			
			
			else if (magnitude > 0) {  //recursive case, if more major lines left to draw
				drawLine(tickLength, (char) magnitude);  //draw major line and its label
				drawnObject.add("[" + String.valueOf(tickLength) + ", " + magnitude + "]");  //add major line # of dashes & magnitude to drawnObject
				drawIntervals(tickLength-1); //call drawIntervals to draw dashes between the 2 major lines
				//add the minus 1 above because if magnitude is 3, then labels are 3 ... 2 ... 1 ... 0
				//and in the in-between spaces we only need the tickLength to be 2 (TWO) because the drawLine(tickLength, 3) takes care of the "---"
				
				engrave(tickLength, magnitude-1); //recursively call engrave() to print the next lower magnitudes and corresponding dashes in between
			                       	   //increment magnitude also helps us reach base case where magnitude is 0 and no more dashes need to be printed
			}	
		}
	
	}  //end of engrave method
	
	
	public void drawIntervals(int tickLength) {
		/**
		 * This method prints  the  intervals  between  two  major lines. Calls drawLine (int tickLength, char tickLabel) to draw the lines.
		 * @param tickLength is length of the major lines.
		 */
		
		if (tickLength > 0) {  //the base case to end recursion is when all the calls have tickLength 0
			drawIntervals(tickLength -1);   //gets to smallest input
			drawnObject.add("[" + String.valueOf(tickLength) + "]");  //stores the number of dashes per line in drawnObject in "[#]" format
			drawLine(tickLength, ' ');    //draws the dashes. The tickLabel is ' ' because no label.
			drawIntervals(tickLength -1);  //recursive: this parts moves us along to the next line, and when appropriate gets us the "-" every other line.
		}		
	}//end of drawIntervals method
	
	
	public void drawLine (int tickLength, char tickLabel) {
		/**
		 * This method draws a line using dash (-) character [i.e., minus sign].
		 * @param tickLength specifies the length of  the  line.
		 * @param tickLabel shows  the  label  that  should  be  printed  next  to  the  line.
		 */
		
		String line = ""; //keeps each set of  dashes, " ", and tickLabel together
		
		if (tickLength == 0) {  //Base case for recursion. All the "-" have been printed, just add " " and the tickLabel. 
			line = line +  " " + tickLabel;
			charPrintCount++;  //Increment charPrintCount. Once for " " and once for tickLabel character.
			charPrintCount++;
		}
		
		if (tickLength > 0) {  //recursive case for when there are still more "-" to be printed
			line = line + "-";
			charPrintCount++;
			drawLine(tickLength-1, tickLabel);  //function calls itself, to print the next "-".
			                                    //tickLength-1 gets us closer to base case and tracks how many "-" left to print.
		}
	}  //end of drawLine method
	
	
	public static int cupSelection(int weight[], int value[], int maxWeight, int index) {
		/**
		 * This method put a number of measuring cups into the box so that the total weight of the cups <= maxWeight and the total price is maximised.
		 *  @param weight[] is  an  array of  integer that  holds  the  weight of each cup.
		 *  @param value[] is also an array of int that holds the price of the cups.
		 *  These two arrays  are  parallel.
		 *  @param maxWeight is  the  highest  weight  that  the  selected  cup(s)  can  have.
		 *  @param index is  the array index. At first call, index holds the length of the arrays.
		 *  @precondition index  n  of  the  first  array  pertains  to  the  same  index  of  the  second array, & the length of both the arrays is the same.
		 *  @post we do not want to have two cups from the same type.
		 *  @return  is  the  total  price  of  the  selection.
		 */
		
		ArrayList<ArrayList<Double>> valuePerWeight = new ArrayList<ArrayList<Double>>();  //make a 2d ArrayList like [ [#,#,#], [#,#,#], [#,#,#] ] fill with for loop
		//ArrayList valuePerWeight stores the [value/weight, weight, value] of each parallel pair of weight and value.
		 for (int i = 0; i < index; i++) {
			 double valueDividedByWeight = (double) value[i] / (double) weight[i];
			 valuePerWeight.add(new ArrayList<Double>(Arrays.asList(valueDividedByWeight, (double) weight[i], (double) value[i])));
		 }

		 Collections.sort(valuePerWeight, new Comparator<ArrayList<Double>>() {  //sort the arrayList from smallest to largest (value divided by weight)
			 @Override
			 public int compare(ArrayList<Double> a, ArrayList<Double> b) {
				 return a.get(0).compareTo(b.get(0));   //sort ArrayList by the first element in each inner array, that's why .get(0)
			 } //end of compare
		 }); //end of Collections.sort

		 
		if (index == 0) {   //base case. recursion ends when no more cups/elements in the weight[] and value[]
			return 0;
		}

		if (valuePerWeight.get(index-1).get(1) <= maxWeight) {    //.get(index-1) gives us the array with info on the most valuable cup by weight
            //.get(1) afterwards fetches the info on just the cup's weight, and check if its less than max weight 
			
			maxWeight = (int) (maxWeight - valuePerWeight.get(index-1).get(1));  //subtract this cup's weight from maxWeight
			
			int totalPrice = (int) (0 + valuePerWeight.get(index-1).get(2));  //add this cup's value to total price

			index = index -1;  //this cup has been added to the box, and the number of cups remaining goes down 1

			int[] newWeightArray = new int[index];   //create new arrays to store the weight[] and value[]
			int[] newValueArray = new int[index];    //but these arrays don't include the cup that we just packed because we can't pack 2 of same type
			
			for (int z = 0; z < index; z++) {  //0 to new index is number of cups in each new array
				double newWeight = valuePerWeight.get(z).get(1);   //get the values from old array
				newWeightArray[z] = (int) newWeight;                 //copy to new array, cast as int
				double newValue = valuePerWeight.get(z).get(2);
				newValueArray[z] = (int) newValue;
			}

			//because a cup has been packed, add cup's price, because we return total price.   recursive call to pack more cups.
			return totalPrice + cupSelection(newWeightArray, newValueArray, maxWeight, index); 
		}

			
			else {  //go to else when the most valuable cup by weight is too heavy to fit
				
				//remove the cup from array, we can't pack/use it. 1 less cup so sub 1 from index...
				index = index -1;   //so NEW arrays weight[] and value[] will each be 1 item shorter
				
				int[] newWeightArray = new int[index];   //same as before, make new arrays weight[] and value[] without this one cup's info
				int[] newValueArray = new int[index];  
				
				for (int z = 0; z < index; z++) {
					double newWeight = valuePerWeight.get(z).get(1);
					newWeightArray[z] = (int) newWeight;
					double newValue = valuePerWeight.get(z).get(2);
					newValueArray[z] = (int) newValue;
				}
				
	//no cup was packed so don't factor in its price or weight. just call the function recursively and try again with the next most valuable by weight cup
				return cupSelection(newWeightArray, newValueArray, maxWeight, index); 
			}

		

	} //end of cupSelection method
	
}//end of class
